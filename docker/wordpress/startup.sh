#!/bin/bash

url="http://localhost"

#Dynamic vars pulled from DB
title=test
admin_user=admin
admin_password=admin
path=/var/www/html
sitename=test
sitenamelowercase=test
tagline=test
city=test
state=test
about="test about"
contact=test
noipfolder=test
newsletter=test
category=test
numberofposts=test
fillerstartdate=test
my_terms_of_service='[my_terms_of_service]'
privacy_policy='[my_privacy_policy]'
contact=' [contact-form-7 id="6" title="Contact form 1"]'

#Create temp files for page content
echo $about > about.txt
echo $my_terms_of_service > my_terms_of_service.txt
echo $privacy_policy > privacy_policy.txt
echo $contact > contact.txt

service mysql start
mysql -u root "-proot" < /bin/mysql_create.sql
/usr/sbin/nginx
service php7.0-fpm start
service proftpd start

#WORDPRESS CONFIG
wp site create --slug=$sitename --path=$path --admin_user=$admin_user --admin_password=$admin_password --admin_email=tyrrellasaurus@yahoo.com --allow-root
wp option update blogdescription $tagline --path=$path --allow-root
wp post delete 1 --allow-root --path=$path
wp post delete 2 --allow-root --path=$path
wp plugin activate email-subscribers --path=$path --allow-root
wp plugin activate contact-form-7 --path=$path --allow-root
wp plugin activate noip-wordpress-plugin --path=$path --allow-root
wp plugin activate auto-terms-of-service-and-privacy-policy --path=$path --allow-root
wp plugin activate quick-featured-images-pro --path=$path --allow-root
wp plugin activate WPRobot3 --path=$path --allow-root
wp option update permalink_structure /blog/%postname%/ --path=$path --allow-root
wp post create about.txt --post_type=page --post_title='About' --path=$path --allow-root
wp post create my_terms_of_service.txt --post_type=page --post_title='Terms and Conditions' --path=$path --allow-root
wp post create privacy_policy.txt --post_type=page --post_title='Privacy Policy' --path=$path --allow-root
wp post create contact.txt --post_type=page --post_title='Contact' --path=$path --allow-root
wp widget delete archives-2 --path=$path --allow-root
wp widget delete recent-comments-2 --path=$path --allow-root
wp widget delete meta-2 --path=$path --allow-root
wp widget add my_terms_of_service sidebar-2 1 --title="my_terms_of_service"
wp widget add privacy_policy sidebar-2 2 --title="privacy_policy"
wp widget add contact sidebar-2 3 --title="contact"

tail -f /var/log/nginx/error.log
